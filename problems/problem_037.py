# Complete the pad_left function which takes three parameters
#   * a number
#   * the number of characters in the result
#   * a padding character
# and turns the number into a string of the desired length
# by adding the padding character to the left of it
#
# Examples:
#   * number: 10
#     length: 4
#     pad:    "*"
#     result: "**10"
#   * number: 10
#     length: 5
#     pad:    "0"
#     result: "10000"
#   * number: 1000
#     length: 3
#     pad:    "0"
#     result: "1000"
#   * number: 19
#     length: 5
#     pad:    " "
#     result: "   19"

def pad_left(number, length, pad):
    str_num = str(number)
    num_length = len(str_num)
    times_to_pad = length - num_length
    result = pad * times_to_pad + str_num
    return result

print(pad_left(19,5," "))

  # str_num = str(number)
    # pad_result = ""
    # num_length = len(str_num)
    # times_to_pad = length - num_length
    # for i in range(times_to_pad):
    #     pad_result = pad_result + pad
    # return pad_result + str_num
