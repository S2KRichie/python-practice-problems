# Write a function that meets these requirements.
#
# Name:       halve_the_list
# Parameters: a single list
# Returns:    two lists, each containing half of the original list
#             if the original list has an odd number of items, then
#             the extra item is in the first list
#
# Examples:
#    * input: [1, 2, 3, 4]
#      result: [1, 2], [3, 4]
#    * input: [1, 2, 3]
#      result: [1, 2], [3]

input = [1,2,3,4,5,6,7,8]
input1 = [1,2,3]
def halve_the_list(list):
    result = []
    result1 = []
    for i in range(len(list)):
        if i < len(list)/2:
            result.append(list[i])
        if i >= len(list)/2:
            result1.append(list[i])
    return result, result1


print(halve_the_list(input))
