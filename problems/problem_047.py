# Complete the check_password function that accepts a
# single parameter, the password to check.
#
# A password is valid if it meets all of these criteria
#   * It must have at least one lowercase letter (a-z)
#   * It must have at least one uppercase letter (A-Z)
#   * It must have at least one digit (0-9)
#   * It must have at least one special character $, !, or @
#   * It must have six or more characters in it
#   * It must have twelve or fewer characters in it
#
# The string object has some methods that you may want to use,
# like ".isalpha", ".isdigit", ".isupper", and ".islower"



def check_password(password):
    contains_upper = False
    contains_lower = False
    contains_digit = False
    contains_special = False
    contains_sixplus = False
    contains_twelveless = False
    if len(password) <= 12 and len(password) >= 6:
         contains_twelveless = True
         contains_sixplus = True
    for char in password:
        if char.isalpha():
            if char.upper():
                contains_upper = True
            if char.lower():
                contains_lower = True
        elif char.isdigit():
                contains_digit = True
        else:
             contains_special = True

    if contains_sixplus & contains_digit & contains_lower & contains_sixplus & contains_twelveless &contains_upper & contains_special:
    # if contains_sixplus == True and contains_digit == True and contains_lower == True and contains_upper == True and contains_special == True and contains_twelveless == True:
        return True
    else:
         return False


print(check_password("Sp@t420"))
