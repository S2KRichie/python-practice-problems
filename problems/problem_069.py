# Write a class that meets these requirements.
#
# Name:       Student
#
# Required state:
#    * name, a string
#
# Behavior:
#    * add_score(score)   # Adds a score to their list of scores
#    * get_average()      # Gets the average of the student's scores
#

#
# There is pseudocode for you to guide you.

# class Student
    # method initializer with required state "name"
        # self.name = name
        # self.scores = [] because its an internal state

class Student:
    def __init__(self, name):
        self.name = name
        self.score = []

    # method add_score(self, score)
        # appends the score value to self.scores

    def add_score(self,score):
        self.score.append(score)

    # method get_average(self)
        # if there are no scores in self.scores
            # return None
        # returns the sum of the scores divided by
        # the number of scores
    def get_average(self):
        if not self.score:
            return None
        sum = 0
        for value in self.score:
            sum = (sum + value)
        return sum/len(self.score)


# Example:
student = Student("Malik")

print(student.get_average())    # Prints None
student.add_score(80)
print(student.get_average())    # Prints 80
student.add_score(90)
student.add_score(82)
print(student.get_average())    # Prints 84
