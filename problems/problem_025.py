# Complete the calculate_sum function which accepts
# a list of numerical values and returns the sum of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#

nums_list = []

def calculate_sum(values):
    sum = 0
    if len(values) == 0:
        return None
    else:
        for nums in values:
            sum += nums
    return sum



sum = calculate_sum(nums_list)

print(sum)
