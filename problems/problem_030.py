# Complete the find_second_largest function which accepts
# a list of numerical values and returns the second largest
# in the list
#
# If the list of values is empty, the function should
# return None
#
# If the list of values has only one value, the function
# should return None
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

dopey_array = []
array = [1,3,4,5,6,10,15]
# append max value to scrap
def find_second_largest(values):
    if not values:
        return None
    output = []
    max_value = max(values)
    for num in values:
        if num < max_value:
            output.append(num)

    return max(output)


print(find_second_largest(array))

    # current_max = max(values)



    # values.sort()
    # return values[-2]
