# Complete the remove_duplicate_letters that takes a string
# parameter "s" and returns a string with all of the
# duplicates removed.
#
# Examples:
#   * For "abc", the result is "abc"
#   * For "abcabc", the result is "abc"
#   * For "abccba", the result is "abc"
#   * For "abccbad", the result is "abcd"
#
# If the list is empty, then return the empty string.

letters = "abc"
dup_letters = "abcabc"
empty_str = ""
def remove_duplicate_letters(s):
    if not s:
        return None
    string = ""
    for letter in s:
        if letter not in string:
            string = string + letter
    return string
print(remove_duplicate_letters(empty_str))
