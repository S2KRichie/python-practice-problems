# Complete the max_in_list function to find the
# maximum value in a list
#
# If the list is empty, then return None.
#

num_list= [1,3,5,7,9,10]
def max_in_list(values):
    if values:
        return max(values)
    return None

print(max_in_list(num_list))
