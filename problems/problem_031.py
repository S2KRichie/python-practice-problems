# Complete the sum_of_squares function which accepts
# a list of numerical values and returns the sum of
# each item squared
#
# If the list of values is empty, the function should
# return None
#
# Examples:
#   * [] returns None
#   * [1, 2, 3] returns 1*1+2*2+3*3=14
#   * [-1, 0, 1] returns (-1)*(-1)+0*0+1*1=2
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

list1 = [1, 2, 3]
list2 = [-1, 0, 1]



def sum_of_squares(values):
    if not values:
        return None
    squared = []
    total = 0
    for num in values:
        squared.append(num ** 2)
    for num in squared:
        total = total + num
    return total

print(sum_of_squares(list1))


# squared = [1, 4, 9]
